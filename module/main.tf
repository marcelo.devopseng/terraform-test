 module "gcs" {
   source  = "../gcs"
   name = var.storage_name
   location = var.location
   storage_class = var.storage_class
}
