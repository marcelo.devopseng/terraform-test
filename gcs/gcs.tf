resource "google_storage_bucket" "my_bucket_test_poc" {
  name          = var.storage_name
  location      = var.location
  storage_class = var.storage_class
}