output "storage_name" {
  value = google_storage_bucket.my_bucket_test_poc.name
}

output "bucket_location" {
  value = google_storage_bucket.my_bucket_test_poc.location
}